# @author: Ashish Shetty aka AlphaSierra
# Fibonacci sequence generator my implementation

fibonacci_list = [0, 1]
last = int(input("Please input the limit of Fibonacci series you want to enlist: "))

def fib():
    m = 0
    n = 1
    i = 0
    for i in range(last):
        p = m + n
        m = n
        n = p
        fibonacci_list.append(p)
    print(fibonacci_list)

fib()
